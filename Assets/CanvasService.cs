using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasService : MonoBehaviour
{
   public static CanvasService Instance { get; private set; }
   public bool IsSpawnSphereBtnPressed => isSpawnSphereBtnPressed;
   
   private bool isSpawnSphereBtnPressed = false;
   
   public void OnSpawnSphereButtonPressed() => isSpawnSphereBtnPressed = true;
   
   public void OnSpawnSphereButtonUnPressed() => isSpawnSphereBtnPressed = false;

   public event Action OnVirusBtnClick;

   private CanvasService() { }
   private void Awake()
   {
      CanvasService[] canvasServices = FindObjectsOfType<CanvasService>();
      if(canvasServices.Length>1)
         Destroy(this);
      Instance = this;
   }

   public void CallOnVirusBtnClick() => OnVirusBtnClick?.Invoke();
}
