using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SphereSpawner : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;

    public IEnumerable<Sphere> ActiveSphereOnScene => activeSphereOnScene;

    private List<Sphere> activeSphereOnScene = new List<Sphere>();
    
    private SphereFactory sphereFactory;
    private void Start()
    {
        sphereFactory = new SphereFactory();
    }

    private bool isSpawnBtnPressed => CanvasService.Instance.IsSpawnSphereBtnPressed;
    private void FixedUpdate()
    {
        if(isSpawnBtnPressed)
            Spawn();
    }

    private void Spawn()
    {
        Vector3 spawnPosition = new Vector3(spawnPoint.position.x+Random.Range(-3,3), spawnPoint.position.y+Random.Range(-0.5f,0.5f), spawnPoint.position.z+Random.Range(-3,3));
        Sphere sphere=sphereFactory.Create(spawnPosition);
        if(sphere!=null)
          activeSphereOnScene.Add(sphere);
    }
    
}
