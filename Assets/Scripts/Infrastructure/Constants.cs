using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
   public const string SpherePrefabPath = "SpherePrefab";
   public const int SpherePoolCount = 100;
}
