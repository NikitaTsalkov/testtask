using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour,IObjectPoolObject<SpherePool>
{
    public bool IsFullInfected => isFullInfected;
    private bool isFullInfected = false;

    public bool IsInfectionStarted => isInfectionStarted;
    private bool isInfectionStarted = false;

    private float fullInfectedTime = 1f;
    
    public SpherePool ObjectPool { get; set; }

    private MeshRenderer meshRenderer;

    private Coroutine lerpColorCoroutine;

    public void ReturnToPoolObject(SpherePool pool)
    {
        ObjectPool.Add(gameObject);
    }

    public void StartVirus()
    {
        if(isInfectionStarted && isFullInfected)
            return;
        
        isInfectionStarted = true;
        StartCoroutine(InfectedCoroutine());
    }

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }
    
    private void Update()
    {
        if (isInfectionStarted)
        {
            if (lerpColorCoroutine == null)
                lerpColorCoroutine = StartCoroutine(LerpColorsOverTime(meshRenderer.material.color,Color.red, fullInfectedTime));
        }
    }

    private IEnumerator InfectedCoroutine()
    {
        yield return new WaitForSeconds(fullInfectedTime);
        isFullInfected = true;
        Debug.Log("Virus Started");
    }
    
    
    private IEnumerator LerpColorsOverTime(Color from, Color to, float time)
    {
        float inversedTime = 1 / time ; 
        for(float step = 0.0f; step < 1.0f ; step += Time.deltaTime * inversedTime )
        {
            meshRenderer.material.color = Color.Lerp(from, to, step);
            yield return null ;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!isFullInfected)
            return;
        
        Sphere sphere = other.GetComponent<Sphere>();
        if (sphere != null)
        {
            sphere.StartVirus();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(!isFullInfected)
            return;
        
        Sphere sphere = other.GetComponent<Sphere>();
        if (sphere != null)
        {
            sphere.StartVirus();
        }
    }
}
