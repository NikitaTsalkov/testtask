using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereFactory 
{
    private SpherePool spherePool;

    public SphereFactory()
    {
        spherePool = new SpherePool();
    }
    
    public Sphere Create(Vector3 at)
    {
        GameObject sphereObj = spherePool.Remove();
        if (sphereObj == null)
            return null;

        Sphere sphere=sphereObj.GetComponent<Sphere>();
        sphere.ObjectPool = spherePool;
        sphereObj.transform.position = at;

        return sphere;
    }
}
