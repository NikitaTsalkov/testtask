using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusSystem : MonoBehaviour
{
    private SphereVirusPicker sphereVirusPicker;
    private void Start()
    {
        sphereVirusPicker = GetComponent<SphereVirusPicker>();
        CanvasService.Instance.OnVirusBtnClick += TryToStartVirus;
    }

    private void OnDisable()
    {
        CanvasService.Instance.OnVirusBtnClick -= TryToStartVirus;
    }

    private void TryToStartVirus()
    {
        Sphere sphere = sphereVirusPicker.Pick();
        if (sphere != null)
        {
            sphere.StartVirus();
        }

    }
}
