using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SphereVirusPicker:MonoBehaviour
{
    [SerializeField] private SphereSpawner sphereSpawner;
    public Sphere Pick()
    {
        IEnumerable<Sphere> spehres = sphereSpawner.ActiveSphereOnScene.Where(sphere =>
            sphere.gameObject.activeInHierarchy  && !sphere.IsInfectionStarted && !sphere.IsFullInfected);

       if (spehres.Count() == 0)
       {
           Debug.Log("Cant find non-infected sphere");
           return null;
       }
       
       return spehres.First();
    }
}
