using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpherePool : IObjectPool
{
   private Queue<GameObject> spherePool = new Queue<GameObject>();
   
   public SpherePool()
   {
      Initialize();
   }

   public void Add(GameObject sphere)
   {
      Sphere sphereObj=sphere.GetComponent<Sphere>();
      if (sphereObj == null)
         return;

      spherePool.Enqueue(sphere.gameObject);
      sphere.gameObject.SetActive(false);
   }

   public GameObject Remove()
   {
      if (spherePool.Count == 0)
      {
         Debug.Log("Pool is empty!");
         return null;
      }
     GameObject obj= spherePool.Dequeue();
     obj.SetActive(true);
     return obj;
   }
   
   private void Initialize()
   {
      for (int i = 0; i < Constants.SpherePoolCount; i++)
      {
         GameObject sphere = GameObject.Instantiate(Resources.Load<GameObject>(Constants.SpherePrefabPath));
         Add(sphere);
         sphere.SetActive(false);
      }
   }
}
