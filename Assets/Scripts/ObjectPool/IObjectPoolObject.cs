using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectPoolObject<T> where T:IObjectPool
{
   T ObjectPool { get; set; }
   void ReturnToPoolObject(T pool);
}
